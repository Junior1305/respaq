﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess
{
   public class _adoSQL
    {
        private string CadenaConexion;
        public _adoSQL(string _CadenaConexion)
        {
            CadenaConexion = ConfigurationManager.ConnectionStrings[_CadenaConexion].ConnectionString;
        }

        public string executeCommand(string nombreSP)
        {
            string rpta = "";
            using (SqlConnection con = new SqlConnection(CadenaConexion))
            {
                try
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand(nombreSP, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    object data = cmd.ExecuteScalar();
                    if (data != null) rpta = data.ToString();

                }
                catch (Exception ex)
                {
                    rpta = ex.Message;
                }
            }
            return rpta;
        }

        public string executeCommandParameter(string nombreSP, string parameter)
        {
            string rpta = "";
            using (SqlConnection con = new SqlConnection(CadenaConexion))
            {
                try
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand(nombreSP, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@params", parameter);
                    object data = cmd.ExecuteScalar();
                    if (data != null) rpta = data.ToString();

                }
                catch (Exception ex)
                {
                    rpta = ex.Message;
                }
            }
            return rpta;
        }

        public string executeCommandParameterLog(string nombreSP, string parameter, string log) //insert 1 , update 2 , delete 3 , login 4 , logoff 5
        {
            string rpta = "";
            using (SqlConnection con = new SqlConnection(CadenaConexion))
            {
                try
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand(nombreSP, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@params", parameter);
                    cmd.Parameters.AddWithValue("@log", log);
                    object data = cmd.ExecuteScalar();
                    if (data != null) rpta = data.ToString();

                }
                catch (Exception ex)
                {
                    rpta = ex.Message;
                }
            }
            return rpta;
        }
    }
}
