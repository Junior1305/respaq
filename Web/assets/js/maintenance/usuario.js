﻿var console = (function () {
    //Solo numeros -- Validacion
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $("input[name='us_tipoUsuario']").change(function () {
        if ($(this).val() == '2') {
            $('.tb_proveedor').hide();
            $('.tb_cliente').show();
        } else {
            $('.tb_cliente').hide();
            $('.tb_proveedor').show();
        }
    });

    $("#us_docIdentidad_ruc").keypress(function (e) {
        if (event.which == 13 || event.keyCode == 13) {
            BuscarSunat();
        }
    })

}());

function InsertUsuario() {
    $("#ftco-loader").addClass('show');
    if (validateInputs('form_usuario_new')) {
        var DocIdentidad = "";
        if (document.getElementById('us_docIdentidad').value != '') {
            DocIdentidad = document.getElementById('us_docIdentidad').value;
        } else {
            DocIdentidad = document.getElementById('us_docIdentidad_ruc').value;
        }
        var usuario = {};
        usuario.us_login = document.getElementById('us_login').value;
        usuario.us_pass = document.getElementById('us_pass').value;
        usuario.us_docIdentidad = DocIdentidad;
        usuario.perfil_id = getRadioValue("us_tipoUsuario");
        usuario.us_nombre = document.getElementById('us_nombre').value;
        usuario.us_apellido = document.getElementById('us_apellido').value;
        usuario.us_razonSocial = document.getElementById('us_razonSocial').value;
        usuario.us_direccion = document.getElementById('us_direccion').value;
        usuario.us_email = document.getElementById('us_email').value;
        usuario.us_telefono = document.getElementById('us_telefono').value;
        usuario.us_estado = '1';
        usuario.us_userUpdate = '';
        var json = JSON.stringify(usuario);
        $.ajax({
            url: "http://127.0.0.1:8000/api/createUser/",
            type: "POST",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#ftco-loader").removeClass('show');
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Mensaje',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Usuario Registrado Correctamente</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class',
                            action: function () {
                                location.href = '/';
                            }
                        }
                    }
                });
            },
            error: function (xhr, status) {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>No se pudo realizar el registro Correctamente. Error</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        });
    } else{
        $("#ftco-loader").removeClass('show');
    }
}

function loginUsuario() {
    $("#ftco-loader").addClass('show');
    if (validateInputs('form_usuario_login')) {
        var usuario = {};
        usuario.us_login = document.getElementById('us_login_path').value;
        usuario.us_pass = document.getElementById('us_pass_path').value;
        if (document.getElementById('us_tipo').checked) {
            usuario.us_tipo = '2';
        } else {
            usuario.us_tipo = '1';
        }
        var json = JSON.stringify(usuario);
        $.ajax({
            url: "http://127.0.0.1:8000/api/login/",
            type: "POST",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jQxhr) {
                $("#ftco-loader").removeClass('show');
                if (data['success']) {
                    var log_detail = data['resp']['0']['us_razonSocial'] != '' ? data['resp']['0']['us_razonSocial'] : data['resp']['0']['us_nombre'] + ' ' + data['resp']['0']['us_apellido'];
                    sendServer({
                        method: "GET",
                        url: "../Main/validarLogin",
                        params: {
                            usuario_id: data['resp']['0']['id'],
                            perfil_id: data['resp']['0']['perfil_id'],
                            nom_usuario: log_detail
                        }
                    }).then(function (result) {
                        location.href = result.responseText;
                    });
                } else {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '50%',
                        useBootstrap: false,
                        title: 'Alerta',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>'+data['message']+'</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        }
                    });
                }
            },
            error: function (xhr, status) {
                $("#ftco-loader").removeClass('show');
                $.alert({
                    closeIcon: true,
                    boxWidth: '50%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Usuario no encontrado</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        });
    } else {
        $("#ftco-loader").removeClass('show');
    }
}

function BuscarSunat() {
     $("#ftco-loader").addClass('show');
    var tipo = document.getElementById('us_docIdentidad_ruc').value;
    sendServer({
        method: "GET",
        url: "../Main/getSunat",
        params: {
            ruc: tipo,
            tipo: tipo.substr(0, 2)
        }
    }).then(function (result) {
        var data = result.responseText;
        var matriz = data.split('|');
        if (data.length > 0) {
            if (tipo.substr(0, 2) != '10' && tipo.substr(0, 2) != '15' && tipo.substr(0, 2) != '17') {
                document.getElementById('us_razonSocial').value = matriz[1];
                document.getElementById('us_direccion').value = matriz[2];
            } else {
                if (matriz[1] == '-') {
                    document.getElementById('us_razonSocial').value = matriz[0].replace('<td class="bg" colspan=3>', '');
                } else {
                    document.getElementById('us_razonSocial').value = matriz[1];
                }
                document.getElementById('us_direccion').value = matriz[2];
            }
        } else {
            $("#ftco-loader").removeClass('show');
            $.alert({
                closeIcon: true,
                boxWidth: '50%',
                useBootstrap: false,
                title: 'Mensaje',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>Conexion Perdida, Intente de Nuevo.</li><li>En caso de que persista, espere unos 10 min para realizar la consulta a la sunat.</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }

            });
        }
        $("#ftco-loader").removeClass('show');
    });
}
