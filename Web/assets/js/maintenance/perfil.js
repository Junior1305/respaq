﻿function loadMantPerfil(urlView, urlLoading, idContent) {
    $("#loaderOverlay").show();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", urlView);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("contenido").innerHTML = xmlhttp.responseText;
            document.getElementById("contenido").style.display = "block";
            sendServer({
                method: "POST",
                url: "../Perfil/list_perfil",
                params: {
                    nombre: "",
                    estado: 0
                }
            }).then(function (result) {
                armarlistarPerfil(result.responseText);
                $('[data-toogle="tooltip"]').tooltip();
                $("#loaderOverlay").hide();
            });
        }
    }
}

function nuevoPerfil() {
    document.getElementById("modalMessageTitle").innerHTML = "Nuevo Perfil";
    $('#modalPerfil').modal(mShow);
    cargarTreeView('0');
    insertInput('modalPerfil');
}

function cargarTreeView(id) {
    sendServer({
        method: "POST",
        url: "../Perfil/list_treeView",
        params: {
            id: id,
        }
    }).then(function (result) {
        armarTreeView(result.responseText);
    }).catch(function (error) {
        console.log('Ocurrió un error: ', error);
    });
}

function armarTreeView(rpta) {
    var contenido = "";
    var id, idMenu;
    if (rpta != "") {
        var resultado = rpta.split("*");
        var campos;
        contenido += "<ul class=\"filetree treeview\" id=\"browser\">";
        for (var i = 0; i < resultado.length; i++) {
            campos = resultado[i].split("~");
            if (campos[0] == campos[1]) {
                contenido += "<li class=\"expandable\"><div class=\"hitarea expandable-hitarea\"></div>";
                contenido += "<span><input type=\"checkbox\" value=\"" + campos[1] + "\" name=\"menu\"";
                if (campos[3] != "0") {
                    contenido += " checked=\"checked\" />";
                } else {
                    contenido += " />";
                }
                contenido += campos[2];
                contenido += "</span>";
                contenido += "<ul style=\"display: none;\">";
                contenido += pintarSubTreeView(resultado, campos[1]);
                contenido += "</ul>";
                contenido += "</li>";
            }
        }
        contenido += "</ul>";
    }
    document.getElementById("menu").innerHTML = contenido;
    $("#browser").treeview();
}

function pintarSubTreeView(resultado, idPadre) {
    var idMenu;
    var campos;
    var contenido = "";
    for (var i = 0; i < resultado.length; i++) {
        campos = resultado[i].split("~");
        if (idPadre == campos[1] && campos[0] != campos[1]) {
            contenido += "<li class=\"last\">";
            contenido += "<span><input type=\"checkbox\" value=\"" + campos[0] + "\" name=\"menu\""
            if (campos[3] != "0") {
                contenido += "checked=\"checked\" />";
            } else {
                contenido += "/>";
            }
            contenido += campos[2];
            contenido += "</span></li>";
        }
    }
    return contenido;
}

function armarlistarPerfil(rpta) {
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    if (document.getElementById('tblListaPerfil').firstElementChild != null) {
        $("#tblListaPerfil").dataTable().fnDestroy();
    }

    if (rpta != "") {
        var contenido = "";
        var registros = rpta.split("*");
        var campo;
        var dataSet = [];
        var option = "<a onclick=\"EliminarPerfil(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" ><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarPerfil(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarPerfil(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\"><i class=\"fa fa-pencil-square-o\"></i></a>";
        for (var i = 0; i < registros.length; i++) {
            var Valores = [];
            campo = registros[i].split("|");
            for (var j = 0; j < campo.length; j++) {
                Valores.push(campo[j]);
            }
            Valores.push(option);
            dataSet.push(Valores);
        }
        $('#tblListaPerfil').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Perfil" },
                { title: "Nombre" },
                { title: "Estado" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });
    } else {
        document.getElementById("tblListaPerfil").innerHTML = "";

        $('#tblListaPerfil').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Perfil" },
                { title: "Nombre" },
                { title: "Estado" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });

    }
}

function AccionPerfil(ind) {
    if (ind == 'I') {
        if (validateInputs('modalPerfil')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Perfil/search_perfil",
                params: {
                    perfil: document.getElementById('txtNombre').value.toUpperCase()
                }
            }).then(function (result) {
                armarSearchPerfil(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        if (validateInputs('modalPerfil')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Perfil/search_perfil_id",
                params: {
                    id: document.getElementById('PerfilIDHid').value,
                    perfil: document.getElementById('txtNombre').value.toUpperCase()
                }
            }).then(function (result) {
                armarSearchPerfil(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    }
}

function armarSearchPerfil(rpta, ind) {
    if (rpta == '0') {
        if (ind == 'I') {
            sendServerLog({
                method: "POST",
                url: "../Perfil/new_perfil",
                params: {
                    nombre: document.getElementById('txtNombre').value.toUpperCase(),
                    estado: getRadioValue('estado'),
                    menu: getCheckMenu('menu'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalPerfil').modal(mHide);
                armarCondPerfil(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        } else {
            sendServerLog({
                method: "POST",
                url: "../Perfil/update_perfil",
                params: {
                    id: document.getElementById('PerfilIDHid').value,
                    nombre: document.getElementById('txtNombre').value.toUpperCase(),
                    estado: getRadioValue('estado'),
                    menu: getCheckMenu('menu'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalPerfil').modal(mHide);
                armarCondPerfil(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Perfil existente, Verifique en la lista de perfiles.</li><li>En caso de que el error persista, contactarse al area de Soporte.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }

}

function getCheckMenu(id) {
    var checks = document.getElementsByName(id);
    var rpta = '';
    for (var i = 0, length = checks.length; i < length; i++) {
        if (checks[i].checked) {
            rpta += checks[i].value+',';
        }
    }
    rpta = rpta.substring(0, rpta.length - 1);
    return rpta;
}

function armarCondPerfil(rpta) {
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>' + rpta + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        },
        onClose: function () {
            nuevo('../Perfil/mant_perfil', 'Perfil', '1', 'loadMantPerfil');
        }
    });
}

function ClearPerfil() {
    clearInput('FormularioPerfil');
    filtrarListaPerfil();
}

function filtrarListaPerfil() {
    $("#loaderOverlay").show();
    sendServer({
        method: "POST",
        url: "../Perfil/list_perfil",
        params: {
            nombre: document.getElementById('txtNombre_search').value,
            estado: document.getElementById('selectEstado_search').value
        }
    }).then(function (result) {
        armarlistarPerfil(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function EliminarPerfil(elemento) {
    var row = elemento.parentNode.parentNode;
    $.confirm({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Eliminar el perfil?</li><li>Se eliminaran las opciones asignadas. Esta seguro?</li><li>Si en caso no se eliminara es porque hay usuarios asignados a ese perfil.</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#loaderOverlay").show();
                sendServerLog({
                    method: "POST",
                    url: "../Perfil/delete_perfil",
                    params: {
                        id: row.children[0].innerHTML
                    }
                }).then(function (result) {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '25%',
                        useBootstrap: false,
                        title: 'Mensaje',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>' + result.responseText + '</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        },
                        onClose: function () {
                            return sendServer({
                                method: "POST",
                                url: "../Perfil/list_perfil",
                                params: {
                                    nombre: document.getElementById('txtNombre_search').value,
                                    estado: document.getElementById('selectEstado_search').value
                                }
                            }).then(function (result) {
                                armarlistarPerfil(result.responseText);
                                $("#loaderOverlay").hide();
                            });
                        }
                    });
                });
            },
            Cancelar: function () {

            }
        }
    });
}

function VisualizarPerfil(elemento) {
    var row = elemento.parentNode.parentNode;
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Visualizar Perfil";
    $('#modalPerfil').modal(mShow);
    cargarTreeView(row.children[0].innerHTML);
  
    sendServer({
        method: "POST",
        url: "../Perfil/show_perfil",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarPerfil(result.responseText);
        viewInput('modalPerfil');
        $("#loaderOverlay").hide();
    });
}

function armarPerfil(rpta) {
    var data = rpta.split("|");
    document.getElementById('PerfilIDHid').value = data[0];
    document.getElementById('txtNombre').value = data[1];
    putRadioValue('estado', data[2]);
}

function EditarPerfil(elemento) {
    var row = elemento.parentNode.parentNode;
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Editar Perfil";
    $('#modalPerfil').modal(mShow);
    editInput('modalPerfil');
    cargarTreeView(row.children[0].innerHTML);
    sendServer({
        method: "POST",
        url: "../Perfil/show_perfil",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarPerfil(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarPerfil(rpta) {
    var data = rpta.split("|");
    document.getElementById('PerfilIDHid').value = data[0];
    document.getElementById('txtNombre').value = data[1];
    putRadioValue('estado', data[2]);
}