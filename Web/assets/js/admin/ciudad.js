﻿var console = (function () {
    $('body,html').stop(true, true).animate({
        scrollTop: 900
    }, 1000);
    //Solo numeros -- Validacion
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $.ajax({
        url: "http://127.0.0.1:8001/api/listCiudad/",
        type: "GET",
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            armarlistarCiudad(data["resp"]);
        },
        error: function (xhr, status) {
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Alerta',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
        }
    });

}());

function armarlistarCiudad(rpta) {

    if (document.getElementById('tblListaCiudad').firstElementChild != null) {
        $("#tblListaCiudad").dataTable().fnDestroy();
    }

    if (rpta != "") {
        var campo;
        var dataSet = [];
        var option = "<a onclick=\"EliminarCiudad(this);\" class=\"btn btn-outline-secondary  btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a>  <a onclick=\"EditarCiudad(this);\" class=\"btn btn-outline-secondary  btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a>";

        Object.keys(rpta).forEach(function (k) {
            var Valores = [];
            Valores.push(rpta[k]['id']);
            Valores.push(getPais(rpta[k]['pais_id']));
            Valores.push(rpta[k]['ci_nombre']);
            Valores.push(rpta[k]['ci_registerDate']);
            Valores.push(option);
            dataSet.push(Valores);
        });

        $('#tblListaCiudad').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Ciudad" },
                { title: "Nombre" },
                { title: "Pais" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column text-center"
                },
                {
                    "targets": [0],
                    "searchable": false
                },
                {
                    "targets": [1, 2, 3, 4],
                    className: "text-center"
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12'p>>"
        });
    } else {
        document.getElementById("tblListaCiudad").innerHTML = "";

        $('#tblListaCiudad').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Pais" },
                { title: "Nombre" },
                { title: "Pais" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                },
                {
                    "targets": [1, 2, 3, 4],
                    className: "text-center"
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12'p>>"
        });

    }
}

function getPais(id) {
    var pais = {};
    pais.id = id;
    var json = JSON.stringify(pais);
    $.ajax({
        url: "http://127.0.0.1:8001/api/getPais/",
        type: "POST",
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: json,
        success: function (data) {
            var fill = data['resp'];
            return fill['pa_nombre'];
        },
        error: function (xhr, status) {
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Alerta',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
        }
    });
}

function EliminarCiudad(elemento) {
    var row = elemento.parentNode.parentNode;
    var ciudad = {};
    ciudad.id = row.children[0].innerHTML;
    var json = JSON.stringify(ciudad);
    $.confirm({
        closeIcon: true,
        boxWidth: '50%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Eliminar la ciudad?</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#ftco-loader").addClass('show');
                $.ajax({
                    url: "http://127.0.0.1:8001/api/deleteCiudad/",
                    type: "DELETE",
                    crossDomain: true,
                    data: json,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $.alert({
                            closeIcon: true,
                            boxWidth: '50%',
                            useBootstrap: false,
                            title: 'Alerta',
                            typeAnimated: true,
                            content: '<ul class="jconfirm-error-list"><li>' + data['message'] + '</li></ul>',
                            buttons: {
                                Aceptar: {
                                    btnClass: 'btn any-other-class',
                                    action: function () {
                                        location.href = '/Admin/lista_ciudad';
                                    }
                                }
                            }
                        });
                        $("#ftco-loader").removeClass('show');
                    },
                    error: function (xhr, status) {
                        $.alert({
                            closeIcon: true,
                            boxWidth: '50%',
                            useBootstrap: false,
                            title: 'Alerta',
                            typeAnimated: true,
                            content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                            buttons: {
                                Aceptar: {
                                    btnClass: 'btn any-other-class',
                                    action: function () {
                                        location.href = '/Admin/lista_ciudad';
                                    }
                                }
                            }
                        });
                        $("#ftco-loader").removeClass('show');
                    }
                });
            },
            Cancelar: function () {

            }
        }
    });
}

function EditarCiudad(elemento) {
    var row = elemento.parentNode.parentNode;
    location.href = '/Admin/form_ciudad?id=' + row.children[0].innerHTML;
}