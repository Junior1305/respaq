﻿var console = (function () {
    $('body,html').stop(true, true).animate({
        scrollTop: 900
    }, 1000);
    var parts = window.location.search.substr(1).split("&");
    var $_GET = {};
    for (var i = 0; i < parts.length; i++) {
        var temp = parts[i].split("=");
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }
    //Solo numeros -- Validacion
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    var usuario = {};
    usuario.id = $_GET['id'];
    var json = JSON.stringify(usuario);
    $.ajax({
        url: "http://127.0.0.1:8000/api/getUser/",
        type: "POST",
        crossDomain: true,
        data: json,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            cargarDataUsuario(data["resp"]);
        },
        error: function (xhr, status) {
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Alerta',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
        }
    });

}());

function cargarDataUsuario(rpta) {
    document.getElementById('us_id').value = rpta['id'];
    document.getElementById('us_login').value = rpta['us_login'];
    document.getElementById('us_pass').value = rpta['us_pass'];
    if (rpta['perfil_id'] == '2') {
        document.getElementById('radCliente').checked = true;
        document.getElementById('us_nombre').value = rpta['us_nombre'];
        document.getElementById('us_apellido').value = rpta['us_apellido'];
        document.getElementById('us_docIdentidad').value = rpta['us_docIdentidad'];
        $('.tb_proveedor').hide();
        $('.tb_cliente').show();
    } else {
        document.getElementById('radProveedor').checked = true;
        document.getElementById('us_razonSocial').value = rpta['us_razonSocial'];
        document.getElementById('us_docIdentidad_ruc').value = rpta['us_docIdentidad'];
        $('.tb_cliente').hide();
        $('.tb_proveedor').show();
    }
    document.getElementById('us_direccion').value = rpta['us_direccion'];
    document.getElementById('us_email').value = rpta['us_email'];
    document.getElementById('us_telefono').value = rpta['us_telefono'];
   
}

function UpdateUsuario() {
    $("#ftco-loader").addClass('show');
    if (validateInputs('form_usuario_new')) {
        var DocIdentidad = "";
        if (document.getElementById('us_docIdentidad').value != '') {
            DocIdentidad = document.getElementById('us_docIdentidad').value;
        } else {
            DocIdentidad = document.getElementById('us_docIdentidad_ruc').value;
        }
        var usuario = {};
        usuario.id = document.getElementById('us_id').value;
        usuario.us_login = document.getElementById('us_login').value;
        usuario.us_pass = document.getElementById('us_pass').value;
        usuario.us_docIdentidad = DocIdentidad;
        usuario.perfil_id = getRadioValue("us_tipoUsuario");
        usuario.us_nombre = document.getElementById('us_nombre').value;
        usuario.us_apellido = document.getElementById('us_apellido').value;
        usuario.us_razonSocial = document.getElementById('us_razonSocial').value;
        usuario.us_direccion = document.getElementById('us_direccion').value;
        usuario.us_email = document.getElementById('us_email').value;
        usuario.us_telefono = document.getElementById('us_telefono').value;
        usuario.us_estado = '1';
        usuario.us_userUpdate = document.getElementById('idUsuario_hid').value;
        var json = JSON.stringify(usuario);
        $.ajax({
            url: "http://127.0.0.1:8000/api/updateUser/",
            type: "PUT",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#ftco-loader").removeClass('show');
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Mensaje',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Usuario Actualizado Correctamente</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class',
                            action: function () {
                                location.href = '/Admin/lista_usuario';
                            }
                        }
                    }
                });
            },
            error: function (xhr, status) {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>No se pudo realizar el registro Correctamente. Error</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        });
    } else {
        $("#ftco-loader").removeClass('show');
    }
}
