﻿var console = (function () {
    $('body,html').stop(true, true).animate({
        scrollTop: 900
    }, 1000);

    var parts = window.location.search.substr(1).split("&");
    var $_GET = {};
    for (var i = 0; i < parts.length; i++) {
        var temp = parts[i].split("=");
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }

    //Cargar de Data Ciudad
    $.ajax({
        url: "http://127.0.0.1:8001/api/listPais/",
        type: "GET",
        crossDomain: true,
        data: json,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            cargarSelectPais(data["resp"]);
        },
        error: function (xhr, status) {
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Alerta',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
        }
    });

    if ($_GET['id'] != null) {
        var ciudad = {};
        ciudad.id = $_GET['id'];
        var json = JSON.stringify(ciudad);
        $.ajax({
            url: "http://127.0.0.1:8001/api/getCiudad/",
            type: "POST",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                cargarDataCiudad(data["resp"]);
            },
            error: function (xhr, status) {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        });
    }

    //Solo numeros -- Validacion
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });



}());

function cargarDataCiudad(rpta) {
    document.getElementById('ci_id').value = rpta['id'];
    document.getElementById('ci_nombre').value = rpta['ci_nombre'];
    document.getElementById('pais_id').value = rpta['pais_id'];
}

function cargarSelectPais(rpta) {
    var data = '';
    data += '<option value="0">Seleccione</option>';
    Object.keys(rpta).forEach(function (k) {
        data += '<option value="' + rpta[k]['id'] + '">' + rpta[k]['pa_nombre'] + '</option>';
    });
    document.getElementById('pais_id').innerHTML = data;
}

function UpdateCiudad() {
    $("#ftco-loader").addClass('show');
    if (validateInputs('form_ciudad_new')) {
        var ciudad = {};
        ciudad.id = document.getElementById('ci_id').value;
        ciudad.ci_nombre = document.getElementById('ci_nombre').value;
        ciudad.pais_id = document.getElementById('pais_id').value;
        ciudad.ci_estado = '1';
        ciudad.ci_userUpdate = document.getElementById('idUsuario_hid').value;
        var json = JSON.stringify(ciudad);
        $.ajax({
            url: "http://127.0.0.1:8001/api/updateCiudad/",
            type: "PUT",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#ftco-loader").removeClass('show');
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Mensaje',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Ciudad Actualizado Correctamente</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class',
                            action: function () {
                                location.href = '/Admin/lista_ciudad';
                            }
                        }
                    }
                });
            },
            error: function (xhr, status) {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>No se pudo realizar el registro Correctamente. Error</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        });
    } else {
        $("#ftco-loader").removeClass('show');
    }
}

function NewCiudad() {
    $("#ftco-loader").addClass('show');
    if (validateInputs('form_ciudad_new')) {
        var ciudad = {};
        ciudad.ci_nombre = document.getElementById('ci_nombre').value;
        ciudad.pais_id = document.getElementById('pais_id').value;
        ciudad.ci_estado = '1';
        ciudad.ci_userUpdate = document.getElementById('idUsuario_hid').value;
        var json = JSON.stringify(ciudad);
        $.ajax({
            url: "http://127.0.0.1:8001/api/createCiudad/",
            type: "POST",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#ftco-loader").removeClass('show');
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Mensaje',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Ciudad creado Correctamente</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class',
                            action: function () {
                                location.href = '/Admin/lista_ciudad';
                            }
                        }
                    }
                });
            },
            error: function (xhr, status) {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>No se pudo realizar el registro Correctamente. Error</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        });
    } else {
        $("#ftco-loader").removeClass('show');
    }
}

