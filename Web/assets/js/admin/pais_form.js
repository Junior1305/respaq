﻿var console = (function () {
    $('body,html').stop(true, true).animate({
        scrollTop: 900
    }, 1000);

    var parts = window.location.search.substr(1).split("&");
    var $_GET = {};
    for (var i = 0; i < parts.length; i++) {
        var temp = parts[i].split("=");
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }

    if ($_GET['id'] != null) {
        var pais = {};
        pais.id = $_GET['id'];
        var json = JSON.stringify(pais);
        $.ajax({
            url: "http://127.0.0.1:8001/api/getPais/",
            type: "POST",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                cargarDataPais(data["resp"]);
            },
            error: function (xhr, status) {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }  
        });
    }

    //Solo numeros -- Validacion
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

   

}());

function cargarDataPais(rpta) {
    document.getElementById('pa_id').value = rpta['id'];
    document.getElementById('pa_nombre').value = rpta['pa_nombre'];
 }

function UpdatePais() {
    $("#ftco-loader").addClass('show');
    if (validateInputs('form_pais_new')) {
        var pais = {};
        pais.id = document.getElementById('pa_id').value;
        pais.pa_nombre = document.getElementById('pa_nombre').value;
        pais.pa_estado = '1';
        pais.pa_userUpdate = document.getElementById('idUsuario_hid').value;
        var json = JSON.stringify(pais);
        $.ajax({
            url: "http://127.0.0.1:8001/api/updatePais/",
            type: "PUT",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#ftco-loader").removeClass('show');
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Mensaje',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Pais Actualizado Correctamente</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class',
                            action: function () {
                                location.href = '/Admin/lista_pais';
                            }
                        }
                    }
                });
            },
            error: function (xhr, status) {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>No se pudo realizar el registro Correctamente. Error</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        });
    } else {
        $("#ftco-loader").removeClass('show');
    }
}

function NewPais() {
    $("#ftco-loader").addClass('show');
    if (validateInputs('form_pais_new')) {
        var pais = {};
        pais.pa_nombre = document.getElementById('pa_nombre').value;
        pais.pa_estado = '1';
        pais.pa_userUpdate = document.getElementById('idUsuario_hid').value;
        var json = JSON.stringify(pais);
        $.ajax({
            url: "http://127.0.0.1:8001/api/createPais/",
            type: "POST",
            crossDomain: true,
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#ftco-loader").removeClass('show');
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Mensaje',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Pais creado Correctamente</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class',
                            action: function () {
                                location.href = '/Admin/lista_pais';
                            }
                        }
                    }
                });
            },
            error: function (xhr, status) {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Alerta',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>No se pudo realizar el registro Correctamente. Error</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        });
    } else {
        $("#ftco-loader").removeClass('show');
    }
}

