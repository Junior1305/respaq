﻿var console = (function () {
    $('body,html').stop(true, true).animate({
        scrollTop: 900
    }, 1000);
    //Solo numeros -- Validacion
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $.ajax({
        url: "http://127.0.0.1:8000/api/listUser/",
        type: "GET",
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            armarlistarUsuario(data["resp"]);
        },
        error: function (xhr, status) {
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Alerta',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
        }
    });

}());

function armarlistarUsuario(rpta) {

    if (document.getElementById('tblListaUsuario').firstElementChild != null) {
        $("#tblListaUsuario").dataTable().fnDestroy();
    }
   
    if (rpta != "") {
        var campo;
        var dataSet = [];
        var option = "<a onclick=\"EliminarUsuario(this);\" class=\"btn btn-outline-secondary  btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a>  <a onclick=\"EditarUsuario(this);\" class=\"btn btn-outline-secondary  btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a>";

        Object.keys(rpta).forEach(function (k) {
            var Valores = [];
            Valores.push(rpta[k]['id']);
            Valores.push(rpta[k]['us_login']);
            Valores.push(rpta[k]['perfil_id'] != '2' ? 'Proveedor' : 'Cliente');
            Valores.push(rpta[k]['us_razonSocial'] != '' ? rpta[k]['us_razonSocial'] : rpta[k]['us_nombre'] + ' ' + rpta[k]['us_apellido']);
            Valores.push(rpta[k]['us_direccion']);
            Valores.push(rpta[k]['us_registerDate']);
            Valores.push(option);
            dataSet.push(Valores);
        });
       
        $('#tblListaUsuario').DataTable({
            data : dataSet,
            columns: [
                { title: "Id Usuario" },
                { title: "Usuario" },
                { title: "Perfil" },
                { title: "Razon Social / Nombres" },
                { title: "Direccion" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column text-center"
                },
                {
                    "targets": [0],
                    "searchable": false
                },
                {
                    "targets": [1,2,3,4,5,6],
                    className: "text-center"
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12'p>>"
        });
    } else {
        document.getElementById("tblListaUsuario").innerHTML = "";

        $('#tblListaUsuario').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Usuario" },
                { title: "Usuario" },
                { title: "Perfil" },
                { title: "Razon Social / Nombres" },
                { title: "Direccion" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                },
                {
                    "targets": [1, 2, 3, 4, 5, 6],
                    className: "text-center"
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12'p>>"
        });

    }
}

function EliminarUsuario(elemento) {
    var row = elemento.parentNode.parentNode;
    var usuario = {};
    usuario.id = row.children[0].innerHTML;
    var json = JSON.stringify(usuario);
    $.confirm({
        closeIcon: true,
        boxWidth: '50%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Eliminar el usuario?</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#ftco-loader").addClass('show');
                $.ajax({
                    url: "http://127.0.0.1:8000/api/deleteUser/",
                    type: "DELETE",
                    crossDomain: true,
                    data: json,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $.alert({
                            closeIcon: true,
                            boxWidth: '50%',
                            useBootstrap: false,
                            title: 'Alerta',
                            typeAnimated: true,
                            content: '<ul class="jconfirm-error-list"><li>'+data['message']+'</li></ul>',
                            buttons: {
                                Aceptar: {
                                    btnClass: 'btn any-other-class',
                                    action: function () {
                                        location.href = '/Admin/lista_usuario';
                                    }
                                }
                            }
                        });
                        $("#ftco-loader").removeClass('show');
                    },
                    error: function (xhr, status) {
                        $.alert({
                            closeIcon: true,
                            boxWidth: '50%',
                            useBootstrap: false,
                            title: 'Alerta',
                            typeAnimated: true,
                            content: '<ul class="jconfirm-error-list"><li>No se pudo realizar la conexion correctamente. Error</li></ul>',
                            buttons: {
                                Aceptar: {
                                    btnClass: 'btn any-other-class',
                                    action: function () {
                                        location.href = '/Admin/lista_usuario';
                                    }
                                }
                            }
                        });
                        $("#ftco-loader").removeClass('show');
                    }
                }); 
            },
            Cancelar: function () {

            }
        }
    });
}

function EditarUsuario(elemento) {
    var row = elemento.parentNode.parentNode;
    location.href = '/Admin/form_usuario?id='+row.children[0].innerHTML;
}