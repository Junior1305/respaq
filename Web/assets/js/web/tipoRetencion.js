﻿function loadMantTipoRetencion(urlView, urlLoading, idContent) {
    $("#loaderOverlay").show();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", urlView);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("contenido").innerHTML = xmlhttp.responseText;
            document.getElementById("contenido").style.display = "block";
            sendServer({
                method: "POST",
                url: "../TipoRetencion/list_tipoRetencion",
                params: {
                    codigo: "",
                    estado: 0
                }
            }).then(function (result) {
                armarlistarTipoRetencion(result.responseText);
                $('[data-toogle="tooltip"]').tooltip();
                $("#loaderOverlay").hide();
            });
        }
    }
}

function nuevoTipoRetencion() {
    document.getElementById("modalMessageTitle").innerHTML = "Nuevo Tipo de Retencion";
    $('#modalTipoRetencion').modal(mShow);
    insertInput('modalTipoRetencion');
}

function armarlistarTipoRetencion(rpta) {
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    if (document.getElementById('tblListaTipoRetencion').firstElementChild != null) {
        $("#tblListaTipoRetencion").dataTable().fnDestroy();
    }

    if (rpta != "") {
        var contenido = "";
        var registros = rpta.split("*");
        var campo;
        var dataSet = [];
        var option = "<a onclick=\"EliminarTipoRetencion(this);\" class=\"btn btn-danger btn-table\" title=\"Desactivar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarTipoRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarTipoRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a>";
        for (var i = 0; i < registros.length; i++) {
            var Valores = [];
            campo = registros[i].split("|");
            for (var j = 0; j < campo.length; j++) {
                Valores.push(campo[j]);
            }
            Valores.push(option);
            dataSet.push(Valores);
        }
        $('#tblListaTipoRetencion').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Tipo Retencion" },
                { title: "Codigo" },
                { title: "Nombre" },
                { title: "Correlativo" },
                { title: "Estado" },
                { title: "Opciones" }
            ],
            columnDefs: [
            {
                "targets": [0],
                className: "hide_column"
            },
            {
                "targets": [0],
                "searchable": false
            }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });
    } else {
        document.getElementById("tblListaTipoRetencion").innerHTML = "";

        $('#tblListaTipoRetencion').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Tipo Retencion" },
                { title: "Codigo" },
                { title: "Nombre" },
                { title: "Correlativo" },
                { title: "Estado" },
                { title: "Opciones" }
            ],
            columnDefs: [
            {
                "targets": [0],
                className: "hide_column"
            },
            {
                "targets": [0],
                "searchable": false
            }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });

    }
}

function AccionTipoRetencion(ind) {
    if (ind == 'I') {
        if (validateInputs('modalTipoRetencion')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../TipoRetencion/search_tipoRetencion",
                params: {
                    codigo: document.getElementById('txtcodigoRetencion').value,
                }
            }).then(function (result) {
                armarSearchTipoRetencion(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        if (validateInputs('modalTipoRetencion')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../TipoRetencion/search_tipoRetencion_id",
                params: {
                    id: document.getElementById('tipoRetencionIDHid').value,
                    codigo: document.getElementById('txtcodigoRetencion').value
                }
            }).then(function (result) {
                armarSearchTipoRetencion(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    }
}

function armarSearchTipoRetencion(rpta, ind) {
    if (rpta == '0') {
        if (ind == 'I') {
            sendServerLog({
                method: "POST",
                url: "../TipoRetencion/new_tipoRetencion",
                params: {
                    codigo: document.getElementById('txtcodigoRetencion').value.toUpperCase(),
                    nombre: document.getElementById('txtNombre').value.toUpperCase(),
                    correlativo: document.getElementById('txtCorrelativo').value,
                    empresa: document.getElementById('selectEmpresa').value,
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalTipoRetencion').modal(mHide);
                armarCondTipoRetencion(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        } else {
            sendServerLog({
                method: "POST",
                url: "../TipoRetencion/update_tipoRetencion",
                params: {
                    id: document.getElementById('tipoRetencionIDHid').value,
                    codigo: document.getElementById('txtcodigoRetencion').value.toUpperCase(),
                    nombre: document.getElementById('txtNombre').value.toUpperCase(),
                    correlativo: document.getElementById('txtCorrelativo').value,
                    empresa: document.getElementById('selectEmpresa').value,
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalTipoRetencion').modal(mHide);
                armarCondTipoRetencion(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Tipo Retencion existente, Verifique en la lista de retenciones.</li><li>En caso de que el error persista, contactarse al area de Soporte.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }

}

function armarCondTipoRetencion(rpta) {
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>' + rpta + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        },
        onClose: function () {
            nuevo('../TipoRetencion/mant_tipoRetencion', 'Tipo Retencion', '1', 'loadMantTipoRetencion');
        }
    });
}

function ClearTipoRetencion() {
    clearInput('FormularioTipoRetencion');
    filtrarListaTipoRetencion();
}

function filtrarListaTipoRetencion() {
    $("#loaderOverlay").show();
    sendServer({
        method: "POST",
        url: "../TipoRetencion/list_tipoRetencion",
        params: {
            codigo: document.getElementById('txtCodigoRetencion_search').value,
            estado: document.getElementById('selectEstado_search').value
        }
    }).then(function (result) {
        armarlistarTipoRetencion(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function EliminarTipoRetencion(elemento) {
    var row = elemento.parentNode.parentNode;
    $.confirm({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Desactivar el tipo de Retencion?</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#loaderOverlay").show();
                sendServerLog({
                    method: "POST",
                    url: "../TipoRetencion/delete_tipoRetencion",
                    params: {
                        id: row.children[0].innerHTML
                    }
                }).then(function (result) {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '25%',
                        useBootstrap: false,
                        title: 'Mensaje',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>' + result.responseText + '</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        },
                        onClose: function () {
                            return sendServer({
                                method: "POST",
                                url: "../TipoRetencion/list_tipoRetencion",
                                params: {
                                    codigo: document.getElementById('txtCodigoRetencion_search').value,
                                    estado: document.getElementById('selectEstado_search').value
                                }
                            }).then(function (result) {
                                armarlistarTipoRetencion(result.responseText);
                                $("#loaderOverlay").hide();
                            });
                        }
                    });
                });
            },
            Cancelar: function () {

            }
        }
    });
}

function VisualizarTipoRetencion(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Visualizar Tipo de Retencion";
    $('#modalTipoRetencion').modal(mShow);
    viewInput('modalTipoRetencion');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../TipoRetencion/show_tipoRetencion",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarTipoRetencion(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarTipoRetencion(rpta) {
    var data = rpta.split("|");
    document.getElementById('tipoRetencionIDHid').value = data[0];
    document.getElementById('txtcodigoRetencion').value = data[1];
    document.getElementById('txtNombre').value = data[2];
    document.getElementById('txtCorrelativo').value = data[3];
    document.getElementById('selectEmpresa').value = data[4];
    putRadioValue('estado', data[5]);
}

function EditarTipoRetencion(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Editar Tipo de Retencion";
    $('#modalTipoRetencion').modal(mShow);
    editInput('modalTipoRetencion');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../TipoRetencion/show_tipoRetencion",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarTipoRetencion(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarTipoRetencion(rpta) {
    var data = rpta.split("|");
    document.getElementById('tipoRetencionIDHid').value = data[0];
    document.getElementById('txtcodigoRetencion').value = data[1];
    document.getElementById('txtNombre').value = data[2];
    document.getElementById('txtCorrelativo').value = data[3];
    document.getElementById('selectEmpresa').value = data[4];
    putRadioValue('estado', data[5]);
}