﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class SeccionController : Controller
    {   

        public ActionResult Principal()
        {
            return View();
        }
        // GET: Seccion
        public ActionResult destino()
        {
            return View();
        }

        public ActionResult nosotros()
        {
            return View();
        }

        public ActionResult hotel()
        {
            return View();
        }

        public ActionResult vehiculo()
        {
            return View();
        }

        public ActionResult contacto()
        {
            return View();
        }

        public string Logout()
        {
            string rpta = "";

            Session.Clear();
            Session.Abandon();

            rpta = "../";

            return rpta;
        }
    }
}