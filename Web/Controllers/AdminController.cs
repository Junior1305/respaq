﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult lista_admin()
        {
            return Session["IdUsuario"] != null && Session["tipoUsuario"].ToString() != "1" ? View("~/Views/Shared/Principal.cshtml") : View();
        }
        // GET: Admin
        public ActionResult lista_usuario()
        {
            return Session["IdUsuario"] != null && Session["tipoUsuario"].ToString() != "1" ? View("~/Views/Shared/Principal.cshtml") : View();
        }

        public ActionResult form_usuario()
        {
            return Session["IdUsuario"] != null && Session["tipoUsuario"].ToString() != "1" ? View("~/Views/Shared/Principal.cshtml") : View();
        }
        public ActionResult lista_pais()
        {
            return Session["IdUsuario"] != null && Session["tipoUsuario"].ToString() != "1" ? View("~/Views/Shared/Principal.cshtml") : View();
        }

        public ActionResult form_pais()
        {
            return Session["IdUsuario"] != null && Session["tipoUsuario"].ToString() != "1" ? View("~/Views/Shared/Principal.cshtml") : View();
        }
        public ActionResult lista_ciudad()
        {
            return Session["IdUsuario"] != null && Session["tipoUsuario"].ToString() != "1" ? View("~/Views/Shared/Principal.cshtml") : View();
        }

        public ActionResult form_ciudad()
        {
            return Session["IdUsuario"] != null && Session["tipoUsuario"].ToString() != "1" ? View("~/Views/Shared/Principal.cshtml") : View();
        }
    }
}