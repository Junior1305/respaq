﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class MainController : Controller
    {
        // GET: Main
        CookieContainer cokkie = new CookieContainer();
        public ActionResult plantilla()
        {
            return View();
        }

        public string getSunat()
        {
            string rpta = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=random");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                request.CookieContainer = cokkie;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream mystream2 = response.GetResponseStream();
                StreamReader mystreamReader2 = new StreamReader(mystream2);
                string responseText = mystreamReader2.ReadToEnd();

                long n = Request.InputStream.Length;
                if (n > 0)
                {
                    byte[] buffer = new byte[n];
                    Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                    string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                    string[] matriz = parametro.Split('|');
                    string myurl = @"http://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias?accion=consPorRuc&nroRuc=" + matriz[0] + "&numRnd="+ responseText.ToString();
                    HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create(myurl);
                    myWebRequest.CookieContainer = cokkie;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

                    HttpWebResponse myhttpWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                    Stream mystream = myhttpWebResponse.GetResponseStream();
                    StreamReader mystreamReader = new StreamReader(mystream);
                    string xDat = ""; int pos = 0;
                    while (!mystreamReader.EndOfStream)
                    {
                        xDat = mystreamReader.ReadLine();
                        pos++;
                        if (matriz[1] != "10" && matriz[1] != "15" && matriz[1] != "17")
                        {
                            switch (pos)
                            {
                                case 132: // Contribuyente
                                    rpta = rpta + getDatafromXML(xDat, 0);
                                    break;
                                case 141: //Nombre Comercial
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                                case 171: //DIRECCION
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                            }
                        }
                        else
                        {
                            switch (pos)
                            {
                                case 132: // Distribuidor
                                    rpta = rpta + getDatafromXML(xDat, 0);
                                    break;
                                case 150: //Nombre Comercial
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                                case 183: //DIRECCION
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                            }
                        }

                    }

                    return rpta;
                }

            }
            catch (Exception)
            {
                return "Conexion Perdida, Intente de Nuevo.";
            }

            return rpta;
        }

        private string getDatafromXML(string lineRead, int len = 0)
        {
            try
            {
                lineRead = lineRead.Trim();
                lineRead = lineRead.Remove(0, len);
                lineRead = lineRead.Replace("</td>", "");
                lineRead = lineRead.Replace("<td class=\"bg\" colspan=1>", "");
                lineRead = lineRead.Replace("<td class=\"bg\" colspan=3>", "");
                while (lineRead.Contains("  "))
                {
                    lineRead = lineRead.Replace("  ", " ");
                }
                return lineRead;
            }
            catch (Exception)
            {
                return "-";
            }

        }

        public string validarLogin()
        {
            string rpta = string.Empty;
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('|');
                if (matriz[0].ToString() == "1") { //Admin
                    Session["IdUsuario"] = matriz[0];
                    Session["tipoUsuario"] = matriz[1];
                    Session["nomUsuario"] = matriz[2];
                    rpta = "/Admin/lista_admin";
                }
                else if (matriz[0].ToString() == "2") //Cliente
                {
                    Session["IdUsuario"] = matriz[0];
                    Session["tipoUsuario"] = matriz[1];
                    Session["nomUsuario"] = matriz[2];
                    rpta = "/";
                }
                else if(matriz[0].ToString() == "3") //Proveedor
                {
                    Session["IdUsuario"] = matriz[0];
                    Session["tipoUsuario"] = matriz[1];
                    Session["nomUsuario"] = matriz[2];
                    rpta = "/";
                }
            }
            return rpta;
        }

        public string Logout()
        {
            string rpta = "";

            Session.Clear();
            Session.Abandon();

            rpta = "../";

            return rpta;
        }
    }
    
}