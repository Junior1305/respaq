﻿function irToMetodo(id, nombre, ruta, metodo) {
    window[metodo](ruta, nombre, id);

}

var mShow = { "show": true, "backdrop": "static", "keyboard": false };
var mHide = "hide";//{ "hide": true };

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function cargarMes(id) {
    var data =  '<option value="01">Enero</option>';
    data += '<option value="02">Febrero</option>';
    data += '<option value="03">Marzo</option>';
    data += '<option value="04">Abril</option>';
    data += '<option value="05">Mayo</option>';
    data += '<option value="06">Junio</option>';
    data += '<option value="07">Julio</option>';
    data += '<option value="08">Agosto</option>';
    data += '<option value="09">Septiembre</option>';
    data += '<option value="10">Octubre</option>';
    data += '<option value="11">Noviembre</option>';
    data += '<option value="12">Diciembre</option>';
    $('#' + id).append(data);
}


function cargarAño(id) {
    var data = '';
    var fecha = new Date();
    var ano = parseInt(fecha.getFullYear());
    var anoRestante = ano - 50;
    for (var i = ano; i >= anoRestante; i--) {
        data += '<option value="' + i + '">' + i + '</option>';
    }
    $('#' + id).append(data);
}


function getTipoCambioActual() {
    var tipoCambio = '';
    $.each($('#FormularioTipoCambio table:nth-child(3) tr:last-child() td:last-child()'), function (index, value) {
        tipoCambio = $(this).html().trim();
    });
    return tipoCambio;
}

function hasClass(ele, cls) {
    return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

function addClass(ele, cls) {
    if (!hasClass(ele, cls)) ele.className += " " + cls;
}

function removeClass(ele, cls) {
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
}

function insertInput(id) {
    $('#' + id).find("input[type=text]").val("");
    $('#' + id).find("input[type=password]").val("");
    $('#' + id).find("input[type=email]").val("");
    $('#' + id).find("select").val("");
    $('#' + id).find("input, select").removeAttr('disabled');
    $('#' + id).find("input").removeAttr("checked");
    $('#' + id).find("select").prop('selectedIndex', 0);
    $('.btn-insert').show();
    $('.btn-update').hide();
    $('#' + id).find("input, select").removeClass('red-validate');
    $('#' + id).find("table tbody").empty();
    $('#' + id).find("input").removeClass('red-validate-radio');
}

function viewInput(id) {
    $('.btn-insert').hide();
    $('.btn-update').hide();
    $('#' + id).find("select").prop('selectedIndex', 0);
    $('#' + id).find("input, select").attr("disabled", true);
    $('#' + id).find("input[type=text]").val("");
    $('#' + id).find("input[type=password]").val("");
    $('#' + id).find("input[type=email]").val("");
    $('#' + id).find("select").val("");
    $('#' + id).find("table tbody").empty();
}

function editInput(id) {
    $('#' + id).find("table tbody").empty();
    $('#' + id).find("input[type=text]").val("");
    $('#' + id).find("input[type=password]").val("");
    $('#' + id).find("input[type=email]").val("");
    $('#' + id).find("select").val("");
    $('#' + id).find("input, select").removeAttr('disabled');
    $('#' + id).find("input").removeAttr("checked");
    $('.btn-insert').hide();
    $('.btn-update').show();
    $('#' + id).find("input, select").removeClass('red-validate');
    $('#' + id).find("input").removeClass('red-validate-radio');
}

function clearInput(id) {
    $('#' + id).find("input:not([type=radio])").val("");
    $('#' + id).find("select").prop('selectedIndex', 0);
}

function validateInputs(id) {
    var rpta = true;
    //Texto
    $.each($('#' + id + ' input:not([type=radio]).required'), function (index, value) {
        if (!$(value).val()) {
            rpta = false;
            $(this).addClass('red-validate');
        } else {
            $(this).removeClass('red-validate');
        }
    });
    //Select
    $.each($('#' + id + ' select.required '), function (index, value) {
        if ($(value).val() == 0) {
            rpta = false;
            $(this).addClass('red-validate');
        } else {
            $(this).removeClass('red-validate');
        }
    });
    //Radio
    $.each($('#' + id + ' .required-radio'), function (index, value) {
        if ($(this).find(":checked").length == 0) {
            rpta = false;
            $(this).find("input").addClass('red-validate-radio');
        } else {
            $(this).find("input").removeClass('red-validate-radio');
        }
    });
    //CheckBox
    $.each($('#' + id + ' .required-check'), function (index, value) {
        if ($(this).find(":checked").length == 0) {
            rpta = false;
            $(this).find("input").addClass('red-validate-radio');
        } else {
            $(this).find("input").removeClass('red-validate-radio');
        }
    });


    if (rpta) {
        return true;
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '15%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Por favor completar todos los datos requeridos.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
        return false;
    }
}

function getRadioValue(id) {
    var radios = document.getElementsByName(id);
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            return radios[i].value;
        }
    }
}

function putRadioValue(id,val) {
    var radios = document.getElementsByName(id);
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].value == val) {
            radios[i].checked = true;
        }
    }
}

function SelectMultiple(id,list) {
    var valor = '<ul>';
    var matriz = list.split('*')
    for (var i = 0; i < matriz.length; i++) {
        var arr = matriz[i].split('|');
        valor += '<li><label><input type="checkbox" value="' + arr[0] + '" /><span>' + arr[1] + '</span></label></li>';
    }
    valor += '</ul>';
    $("#" + id + " div div:nth-child(2)").append(valor);
    $("#divAddon").click(function () {
        $("#" + id + " div div:nth-child(2)").fadeToggle(250);
    });

    $(":checkbox", "#" + id + " div div:nth-child(2)").each(function () {
        $(this).bind("click", function () {
            var marcados = []; var marcadosID = [];

            $(":checked", "#" + id + " div div:nth-child(2)").each(function (item, index) {
                marcados.push($(this).next().text());
                marcadosID.push($(this).val());
            });
            console.log(marcadosID);
            $("#" + id + " div  input[type=text]").val(marcados.length == 0 ? "" : marcados.join(","));
            $("#" + id + " div  input[type=hidden]").val(marcadosID.length == 0 ? "" : marcadosID.join(","));
        });
    });
}


function getCheckDinamic(id, arr) {
    var matriz = arr.split(',');
    var count = 0; var data = '';
    $(":checkbox", "#" + id + " div div:nth-child(2)").each(function () {
        for (var i = 0; i < matriz.length; i++) {
            if ($(this).val() == matriz[i]) {
                $(this).prop('checked', true);
                data = data + $(this).next().text() + ',';
            }
        }
    });
    data = data.substr(0, data.length - 1);
    $("#" + id + " div  input[type=text]").val(data);
    $("#" + id + " div  input[type=hidden]").val(arr);
}

function crearCombo(lista, idCombo, primerItem) {
    var contenido = "";
    if (primerItem != null && primerItem != "") {
        contenido += "<option value='0'>";
        contenido += primerItem;
        contenido += "</option>";
    }

    if (lista != null && lista.length > 0) {
        var nRegistros = lista.length;
        var campos;
        for (var i = 0; i < nRegistros; i++) {
            campos = lista[i].split("|");
            contenido += "<option value=\"";
            contenido += campos[0];
            contenido += "\">";
            contenido += campos[1];
            contenido += "</option>";
        }
    }
    var cbo = document.getElementById(idCombo);
    if (cbo != null) {
        cbo.innerHTML = contenido;
    }
}

var promise = new Promise(function (resolve, reject) {
    if (1 == '1') {
        resolve(1);
    }
    else {
        reject('Esta promesa nunca será rechazada');
    }
});

var sendServer = function (opts) {
    var xhr = new XMLHttpRequest;
    return new Promise(function (resolve, reject) {
        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) return;
            if (xhr.status >= 200 && xhr.readyState == 4) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        }
        var params = opts.params;
        if (params && typeof params === 'object') {
            opts.method = "POST";
            params = Object.keys(params).map(function (key) {
                return encodeURIComponent(params[key]);
            }).join('|');
        }
        xhr.open(opts.method || 'GET', opts.url, true);
        xhr.send(params)
    });
}

var sendServerLog = function (opts) {
    var xhr = new XMLHttpRequest;
    return new Promise(function (resolve, reject) {
        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) return;
            if (xhr.status >= 200 && xhr.readyState == 4) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        }
        var params = opts.params; var log = opts.params;
        if (params && typeof params === 'object') {
            opts.method = "POST";
            params = Object.keys(params).map(function (key) {
                return encodeURIComponent(params[key]);
            }).join('|');

            log = Object.keys(log).map(function (key2) {
                return encodeURIComponent(key2 + ': ' + log[key2]);
            }).join(',');
        }
        xhr.open(opts.method || 'GET', opts.url, true);
        var sendr = params + encodeURIComponent('¥') + log;
        xhr.send(sendr)
    });
}

var logout = document.getElementById("cerrarSesion");

logout.onclick = function () {
    var url = "../Security/Logout/";
    $.get(url, mostrarValidarRespuesta);
}

function mostrarValidarRespuesta(rpta) {
    window.location.href = rpta;
}

function autocomplete(inp, arr, target) {
    var currentFocus;
    var entrada = document.getElementById(inp);
    entrada.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        a = document.createElement("div");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        for (i = 0; i < arr.length; i++) {
            var values = arr[i].split('|');
            var dato = new RegExp(val.toUpperCase() + ".*");
            if (values[1].toUpperCase().match(dato)) {
                b = document.createElement("div");
                b.innerHTML = "<strong>" + values[1] + "</strong>";
                b.innerHTML += "<input type='hidden' value='" + values[0] + "'>";
                b.addEventListener("click", function (e) {
                    entrada.value = values[0];
                    document.getElementById(target).value = this.getElementsByTagName("input")[0].value;
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    entrada.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) {
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

(function () {

	/**
	 * Decimal adjustment of a number.
	 *
	 * @param	{String}	type	The type of adjustment.
	 * @param	{Number}	value	The number.
	 * @param	{Integer}	exp		The exponent (the 10 logarithm of the adjustment base).
	 * @returns	{Number}			The adjusted value.
	 */
    function decimalAdjust(type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    // Decimal round
    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    // Decimal floor
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    // Decimal ceil
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }

    Number.prototype.toFixedB = function toFixed(precision) {
        var multiplier = Math.pow(10, precision + 1),
            wholeNumber = Math.floor(this * multiplier);
        return (Math.round(wholeNumber / 10) * 10 / multiplier).toFixed(precision);
    }

    Number.prototype.toFixed10 = function (precision) {
        return Math.round10(this, -precision).toFixed(precision);
    }

})();