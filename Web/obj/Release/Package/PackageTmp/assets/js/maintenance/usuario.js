﻿function loadMantUsuario(urlView, urlLoading, idContent) {
    $("#loaderOverlay").show();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", urlView);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("contenido").innerHTML = xmlhttp.responseText;
            document.getElementById("contenido").style.display = "block";
            var empresas = '1|Bureau Veritas del Peru S.A.*2|Inspectorate Services Peru S.A.C.';
            sendServer({
                method: "POST",
                url: "../Usuario/list_usuario",
                params: {
                    usuario: "",
                    perfil: 0,
                    estado: 0
                }
            }).then(function (result) {
                armarlistarUsuario(result.responseText);
                return sendServer({
                    method: "POST",
                    url: "../Perfil/list_perfil_cbo"
                }).then(function (result) {
                    armarcboPerfil(result.responseText);
                    $('[data-toogle="tooltip"]').tooltip();
                    $("#loaderOverlay").hide();
                    SelectMultiple('selectMultipleEmpresa', empresas);
                });
            });
        }
    }
}




function nuevoUsuario() {
    document.getElementById("modalMessageTitle").innerHTML = "Nuevo Usuario";
    $('#modalUsuario').modal(mShow);
    insertInput('modalUsuario');
}

function armarlistarUsuario(rpta) {
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    if (document.getElementById('tblListaUsuario').firstElementChild != null) {
        $("#tblListaUsuario").dataTable().fnDestroy();
    }

    if (rpta != "") {
        var contenido = "";
        var registros = rpta.split("*");
        var campo;
        var dataSet = [];
        var option = "<a onclick=\"EliminarUsuario(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarUsuario(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarUsuario(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a>";
        for (var i = 0; i < registros.length; i++) {
            var Valores = [];
            campo = registros[i].split("|");
            for (var j = 0; j < campo.length; j++) {
                Valores.push(campo[j]);
            }
            Valores.push(option);
            dataSet.push(Valores);
        }
        $('#tblListaUsuario').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Usuario" },
                { title: "Usuario" },
                { title: "Perfil" },
                { title: "Estado" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });
    } else {
        document.getElementById("tblListaUsuario").innerHTML = "";

        $('#tblListaUsuario').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Usuario" },
                { title: "Usuario" },
                { title: "Perfil" },
                { title: "Estado" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });

    }
}

function armarcboPerfil(rpta) {
    if (rpta != "") {
        lista = rpta.split("*");
        listaCboPerfil(lista);
    }
}

function listaCboPerfil(lista) {
    var Perfil = [];
    var nRegistros = lista.length;
    var campos;
    for (var i = 0; i < nRegistros; i++) {
        campos = lista[i].split("|");
        Perfil.push(campos[0] + "|" + campos[1]);
    }
    crearCombo(Perfil, "selectPerfil", "Seleccione Perfil");
    crearCombo(Perfil, "selectPerfil_search", "Seleccione Perfil");
}

function AccionUsuario(ind) {
    if (ind == 'I') {
        if (validateInputs('modalUsuario')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Usuario/search_usuario",
                params: {
                    usuario: document.getElementById('txtLogin').value,
                }
            }).then(function (result) {
                armarSearchUsuario(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        if (validateInputs('modalUsuario')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Usuario/search_usuario_id",
                params: {
                    id: document.getElementById('usuarioIDHid').value,
                    usuario: document.getElementById('txtLogin').value
                }
            }).then(function (result) {
                armarSearchUsuario(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    }
}

function armarSearchUsuario(rpta, ind) {
    if (rpta == '0') {
        if (ind == 'I') {
            sendServerLog({
                method: "POST",
                url: "../Usuario/new_usuario",
                params: {
                    usuario: document.getElementById('txtLogin').value,
                    pass: document.getElementById('txtPass').value,
                    nombre: document.getElementById('txtNombres').value.toUpperCase(),
                    apellido: document.getElementById('txtApellidos').value.toUpperCase(),
                    email: document.getElementById('txtEmail').value,
                    empresa: document.getElementById('selectEmpresa').value,
                    perfil: document.getElementById('selectPerfil').value,
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalUsuario').modal(mHide);
                armarCondUsuario(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        } else {
            sendServerLog({
                method: "POST",
                url: "../Usuario/update_usuario",
                params: {
                    id: document.getElementById('usuarioIDHid').value,
                    usuario: document.getElementById('txtLogin').value,
                    pass: document.getElementById('txtPass').value,
                    nombre: document.getElementById('txtNombres').value.toUpperCase(),
                    apellido: document.getElementById('txtApellidos').value.toUpperCase(),
                    email: document.getElementById('txtEmail').value,
                    empresa: document.getElementById('selectEmpresa').value,
                    perfil: document.getElementById('selectPerfil').value,
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalUsuario').modal(mHide);
                armarCondUsuario(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Usuario existente, Verifique en la lista de usuarios.</li><li>En caso de que el error persista, contactarse al area de Soporte.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }

}

function armarCondUsuario(rpta) {
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>' + rpta + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        },
        onClose: function () {
            nuevo('../Usuario/mant_usuario', 'Usuario', '1', 'loadMantUsuario');
        }
    });
}

function ClearUsuario() {
    clearInput('FormularioUsuario');
    filtrarListaUsuario();
}

function filtrarListaUsuario() {
    $("#loaderOverlay").show();
    sendServer({
        method: "POST",
        url: "../Usuario/list_usuario",
        params: {
            usuario: document.getElementById('txtUsuario_search').value,
            perfil: document.getElementById('selectPerfil_search').value,
            estado: document.getElementById('selectEstado_search').value
        }
    }).then(function (result) {
        armarlistarUsuario(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function EliminarUsuario(elemento) {
    var row = elemento.parentNode.parentNode;
    $.confirm({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Eliminar el usuario?</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#loaderOverlay").show();
                sendServerLog({
                    method: "POST",
                    url: "../Usuario/delete_usuario",
                    params: {
                        id: row.children[0].innerHTML
                    }
                }).then(function (result) {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '25%',
                        useBootstrap: false,
                        title: 'Mensaje',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>' + result.responseText + '</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        },
                        onClose: function () {
                            return sendServer({
                                method: "POST",
                                url: "../Usuario/list_usuario",
                                params: {
                                    usuario: document.getElementById('txtUsuario_search').value,
                                    perfil: document.getElementById('selectPerfil_search').value,
                                    estado: document.getElementById('selectEstado_search').value
                                }
                            }).then(function (result) {
                                armarlistarUsuario(result.responseText);
                                $("#loaderOverlay").hide();
                            });
                        }
                    });
                });
            },
            Cancelar: function () {

            }
        }
    });
}

function VisualizarUsuario(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Visualizar Usuario";
    $('#modalUsuario').modal(mShow);
    viewInput('modalUsuario');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Usuario/show_usuario",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarUsuario(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarUsuario(rpta) {
    var data = rpta.split("|");
    document.getElementById('usuarioIDHid').value = data[0];
    document.getElementById('txtLogin').value = data[1];
    document.getElementById('txtPass').value = data[2];
    document.getElementById('txtNombres').value = data[3];
    document.getElementById('txtApellidos').value = data[4];
    document.getElementById('txtEmail').value = data[5];
    getCheckDinamic('selectMultipleEmpresa', data[6]);
    document.getElementById('selectPerfil').value = data[7];
    putRadioValue('estado', data[8]);
}

function EditarUsuario(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Editar Usuario";
    $('#modalUsuario').modal(mShow);
    editInput('modalUsuario');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Usuario/show_usuario",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarUsuario(result.responseText);
        $("#loaderOverlay").hide();
    });
}

